<div class="input-field">
  <select class="select2 browser-default" name-"{{$name}}" @isset($id) id="{{$id}}" @endisset>
    <option value="">Niunguna</option>
    @foreach($options as $option)
    <option value="{{$option->id}}">{{$option->name}}</option>
  	@endforeach
  </select>
  {{$slot}}
</div>