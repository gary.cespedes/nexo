<div class="card card card-default scrollspy" @isset($id) id="{{$id}}" @endisset>
  <div class="card-content">
    <h2 class="card-title">{{$title}}</h4>
    {{$slot}}
  </div>
</div>