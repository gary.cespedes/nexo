<p>
    <label>
      <input class="with-gap" name="group1" type="radio" @isset($id) id="{{$id}}" @endisset />
      <span>{{$title}}</span>
    </label>
</p>
