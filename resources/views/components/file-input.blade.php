<div class="file-field input-field" @isset($id) id="{{$id}}" @endisset>
    <div class="btn">
      <span>{{$title}}</span>
      <input type="file" name="{{$name}}" @isset($id) id="{{$id}}" @endisset>
    </div>
    <div class="file-path-wrapper">
      <input class="file-path validate" type="text">
    </div>
</div>
