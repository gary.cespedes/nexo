<!-- Modal Trigger -->
  <a class="waves-effect waves-light btn modal-trigger" href="#modal1">{{$name}}</a>

  <!-- Modal Structure -->
  <div id="modal1" class="modal modal-fixed-footer">
    <div class="modal-content">
      <h4>{{$title}}</h4>
      {{$slot}}
    </div>
    <div class="modal-footer">
      <a href="#!" class="modal-close waves-effect waves-green btn-flat">Cerrar</a>
    </div>
  </div>
