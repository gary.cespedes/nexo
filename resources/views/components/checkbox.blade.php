<p>
    <label>
      <input type="checkbox" class="filled-in" checked="checked" value="{{$value}}" @isset($id) id="{{$id}}" @endisset />
      <span>{{$title}}</span>
    </label>
</p>
