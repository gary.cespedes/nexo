<table class="table-responsive" id="{{$id}}">
  <thead>
    <tr>
      {{$thead}}
    </tr>
  </thead>
  <tbody>
    {{$data}}
  </tbody>
</table>