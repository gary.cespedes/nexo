<div class="row">
 <div class="input-field col s12">
    <label for="{{$name_label}}">{{$text_label}}*</label>
    <input type="{{$type}}" name="{{$name}}" @isset($id) id="{{$id}}" data-error=".error_{{$name}}" @endisset @isset($id) class="{{$id}}" @endisset @isset($placeholder) placeholder="{{$placeholder}}" @endisset @isset($value) value="{{$value}}" @endisset>
    <small class="error_{{$name}}"></small>
  </div>
</div>
