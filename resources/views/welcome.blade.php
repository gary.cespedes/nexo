@extends('layouts.contentLayoutMaster')
@section('content')
<x-row>
    <x-col col="" s="12" m="12" l="12" xl="12">
        <x-card>
            <x-slot name="title">Nuevo Usuario</x-slot>
            <x-form route="#" method="GET" id="formValidate">
                <x-row>
                    <x-col col="" s="12" m="6" l="6" xl="6">
                        <x-input-forms type="text" name="name" id="name">
                            <x-slot name="name_label">user</x-slot>
                            <x-slot name="text_label">Usuario</x-slot>
                        </x-input-forms>
                        <x-input-forms type="email" name="email" id="email">
                            <x-slot name="name_label">email</x-slot>
                            <x-slot name="text_label">Email</x-slot>
                        </x-input-forms>
                    </x-col>
                    <x-col col="" s="12" m="6" l="6" xl="6">
                        <x-input-forms type="password" name="password" id="password">
                            <x-slot name="name_label">password</x-slot>
                            <x-slot name="text_label">Contraseña</x-slot>
                        </x-input-forms>
                        <x-input-forms type="password" name="cpassword" id="cpassword">
                            <x-slot name="name_label">cpassword</x-slot>
                            <x-slot name="text_label">Repita la Contraseña</x-slot>
                        </x-input-forms>
                        <x-button color="purple lightrn-1" type="submit" >
                            Enviar
                            <i class="material-icons right">send</i>
                        </x-button>
                    </x-col>
                </x-row>
            </x-form>
        </x-card>
    </x-col>
</x-row>

@endsection

@section('vendor-scripts')
    <script src="{{asset('js/pages/users/main.js')}}" type="module"></script>
    <script src="{{asset('js/pages/users/crud.js')}}" type="module"></script>
@endsection