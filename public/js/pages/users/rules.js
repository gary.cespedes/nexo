export let rules_user = {
    name: {
        required: true,
        minlength: 5,
        maxlength: 20
    },
    email: {
        required: true,
        email: true,
        maxlength: 30

    },
    password: {
        required: true,
        minlength: 6,
        maxlength: 15

    },
    cpassword: {
        required: true,
        minlength: 6,
        equalTo: "#password",
        maxlength: 15

    },
};

export let messages_user = {

};
