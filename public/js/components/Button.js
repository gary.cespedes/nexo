class Button {
    constructor(data){
        this.data = data;
    }

    initialize(type){
        let return_button;
        let button_delete = `<button id="delete" class="btn waves-effect waves-light red accent-2" data-id = "${this.data}"> <i class="material-icons">delete</i></button>`;
        let button_edit = `<button id="edit" class="btn waves-effect waves-light amber darken-4" data-id = "${this.data}"> <i class="material-icons">edit</i></button>`;
        let button_show = `<button id="show" class="btn waves-effect waves-light purple lightrn-1" data-id = "${this.data}"> <i class="material-icons">visibility</i></button>`;
        let button_config = `<button id="config" class="btn waves-effect waves-light green darken-1" data-id = "${this.data}"> <i class="material-icons">settings</i></button>`;
        switch (type) {
            case 'delete':
                return_button = button_delete;
                break;
            case 'update':
                return_button = button_edit;
                break;
            case 'show':
                return_button = button_show;
                break;
            case 'config':
                return_button = button_config;
                break;
            case 'all':
                return_button = `${button_edit} ${button_delete} ${button_show}`
                break;
            case 'default':
                return_button = ` ${button_edit} ${button_delete} `;
                break;
        }
        return return_button;
    }
}
