<?php

namespace App\Helpers;

use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Log;

class ConnectivityHelper
  {
      private static $keys=[
          '0' => 'kimera',
          '1' => 'trinity',
          '2' => 'false',
          '3' => 'false',
          '4' => 'false',
          '5' => 'false',
          '6' => 'false'
      ];

    static function filter_difference_false(){
        return Arr::where(static::$keys, function ($value) {
            return $value!='false';
        });
    }

    static function get_db_connection_for_user($credentials){

        $keys_available = self::filter_difference_false();
        $db_connection = '';
        foreach($keys_available as $item){
            Config::set('database.default', $item);
            if (Auth::attempt($credentials)){
                $db_connection = $item;
                break;
            }
        }
        return $db_connection;
    }

    static function set_db_connection($db_connection = null){
        if ($db_connection){
            Config::set('database.default', $db_connection);
        }
        else{
            Config::set('database.default', Session::get('db_connection'));
        }
    }

    static function set_user_db($db_connection, $request){

        Config::set('database.default', $db_connection);
        $user = \DB::table('users')
            ->where('email', $request->email)
            ->first();

        Session::put('user', $user);
    }

    static function session_put_db_connection($db_connection){
        Session::put('db_connection', $db_connection);
    }

    static function getKeys(){
        $keys_available = self::filter_difference_false();
        return $keys_available;
    }
}

?>
